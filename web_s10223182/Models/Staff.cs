﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_S10223527.Models
{
    public class Staff
    {
        [Display(Name = "Number of Days")]
        public int NumDays { get; set; }
        [Display(Name = "ID")]
        public int StaffId { get; set; }
        public string Name { get; set; }
        public char Gender { get; set; }
        [Display(Name = "Date of Birth")]
        public DateTime? DOB { get; set; }
        public string Nationality { get; set; }
        [Display(Name ="Email Address")]
        // Custom Validation Attribute for checking email address exists
        [ValidateEmailExists]
        public string Email { get; set; }
        [Display (Name = "Monthly Salary(SGD)")]
        [DisplayFormat(DataFormatString = "{0:#,##0.00}")]
        public decimal Salary { get; set; }
        [Display (Name = "Full-Time Staff")]
        public bool IsFullTime { get; set; }
        [Display(Name ="Branch")]
        public int? BranchNo { get; set; }
    }
}
