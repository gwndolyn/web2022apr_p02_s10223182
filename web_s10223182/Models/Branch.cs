﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace web_S10223527.Models
{
    public class Branch
    {
        [Display (Name="ID")]
        public int BranchNo { get; set; }
        public string Address { get; set; }
        public string Telephone { get; set; }
    }
}
